# ELECTRON NEON TEST

Just a minimal project to integrate neon addons with electron v4.2

WORKS ON MAC
DON'T WORKS ON LINUX

- Install the project : `npm i`
- Run in dev mode : `npm run dev`
- Package for mac : `npm run build -- --mac`
- Package for linux : `npm run build -- --linux`
